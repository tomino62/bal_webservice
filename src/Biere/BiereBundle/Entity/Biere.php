<?php

namespace Biere\BiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * ORM\Table(name="Biere", indexes={@ORM\Index(name="fk_Biere_Couleur_idx", columns={"Couleur_id"}), ORM\Index(name="fk_Biere_Marque1_idx", columns={"Marque_id"})})
 * ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\BiereRepository")
 */
class Biere extends Model\Biere
{
}