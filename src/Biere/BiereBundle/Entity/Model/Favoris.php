<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Biere\BiereBundle\Entity\Favoris
 * @ORM\Table(name="Favoris")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\FavorisRepository")
 */
class Favoris
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var null|\DateTime
     * @ORM\Column(name="create_time", type="datetime", nullable=true, options={})
     */
    protected $createTime;

    /**
     * @var null|\Biere\BiereBundle\Entity\Biere
     * @ORM\ManyToOne(targetEntity="Biere", inversedBy="favoris")
     * @ORM\JoinColumn(name="Biere_id", referencedColumnName="id", nullable=true)
     */
    protected $biere;

    /**
     * @var null|\Biere\BiereBundle\Entity\Bar
     * @ORM\ManyToOne(targetEntity="Bar", inversedBy="favoris")
     * @ORM\JoinColumn(name="Bar_id", referencedColumnName="id", nullable=true)
     */
    protected $bar;

    /**
     * @var \Biere\BiereBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="favoris")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * only construct object
     */
    public function __construct()
    {
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\Favoris
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of createTime.
     *
     * @param null|\DateTime $createTime
     * @return \Biere\BiereBundle\Entity\Favoris
     */
    public function setCreateTime(\DateTime $createTime = null)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get the value of createTime.
     *
     * @return null|\DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set Biere entity (many to one).
     *
     * @param null|\Biere\BiereBundle\Entity\Biere $biere
     * @return \Biere\BiereBundle\Entity\Favoris
     */
    public function setBiere(\Biere\BiereBundle\Entity\Biere $biere = null)
    {
        $this->biere = $biere;

        return $this;
    }

    /**
     * Get Biere entity (many to one).
     *
     * @return null|\Biere\BiereBundle\Entity\Biere
     */
    public function getBiere()
    {
        return $this->biere;
    }

    /**
     * Set Bar entity (many to one).
     *
     * @param null|\Biere\BiereBundle\Entity\Bar $bar
     * @return \Biere\BiereBundle\Entity\Favoris
     */
    public function setBar(\Biere\BiereBundle\Entity\Bar $bar = null)
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     * Get Bar entity (many to one).
     *
     * @return null|\Biere\BiereBundle\Entity\Bar
     */
    public function getBar()
    {
        return $this->bar;
    }

    /**
     * Set User entity (many to one).
     *
     * @param \Biere\BiereBundle\Entity\User $user
     * @return \Biere\BiereBundle\Entity\Favoris
     */
    public function setUser(\Biere\BiereBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User entity (many to one).
     *
     * @return \Biere\BiereBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'         => $this->id,
            'createTime' => $this->createTime ? $this->createTime.'' : $this->createTime
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }
}
