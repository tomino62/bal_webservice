<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Biere\BiereBundle\Entity\Marque
 * @ORM\Table(name="Marque")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\MarqueRepository")
 */
class Marque
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=45, nullable=false, options={})
     */
    protected $nom;

    /**
     * collection of Biere
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Biere[]
     * 
     * @ORM\OneToMany(targetEntity="Biere", mappedBy="marque")
     * @ORM\JoinColumn(name="Marque_id", referencedColumnName="id", nullable=false)
     */
    protected $bieres;

    /**
     * only construct object
     */
    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of nom.
     *
     * @param string $nom
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add Biere entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Biere $biere
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function addBiere(\Biere\BiereBundle\Entity\Biere $biere)
    {
        $biere->setMarque($this);
        $this->bieres[] = $biere;

        return $this;
    }

    /**
     * remove Biere entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Biere $biere
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function removeBiere(\Biere\BiereBundle\Entity\Biere $biere)
    {
        $this->bieres->removeElement($biere);

        return $this;
    }

    /**
     * Get Biere entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Biere[]
     */
    public function getBieres()
    {
        return $this->bieres;
    }

    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'            => $this->id,
            'nom'           => $this->nom,
            'descriptionFr' => $this->descriptionFr,
            'descriptionEn' => $this->descriptionEn
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }
}
