<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Biere\BiereBundle\Entity\User
 * @ORM\Table(name="User")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="gerant", type="boolean", nullable=true, options={})
     */
    protected $gerant;

    /**
     * collection of Favoris
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Favoris[]
     * 
     * @ORM\OneToMany(targetEntity="Favoris", mappedBy="user")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id", nullable=false)
     */
    protected $favoris;

    /**
     * @var null|\Biere\BiereBundle\Entity\Bar
     * @ORM\ManyToOne(targetEntity="Bar", inversedBy="users")
     * @ORM\JoinColumn(name="Bar_id", referencedColumnName="id", nullable=true)
     */
    protected $bar;

    /**
     * only construct object
     */
    public function __construct()
    {
		parent::__construct();
        $this->favoris = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of gerant.
     *
     * @param boolean $gerant
     * @return \Biere\BiereBundle\Entity\User
     */
    public function setGerant($gerant)
    {
        $this->gerant = $gerant;

        return $this;
    }

    /**
     * Get the value of gerant.
     *
     * @return boolean
     */
    public function getGerant()
    {
        return $this->gerant;
    }

    /**
     * Add Favoris entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\User
     */
    public function addFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $favoris->setUser($this);
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * remove Favoris entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\User
     */
    public function removeFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);

        return $this;
    }

    /**
     * Get Favoris entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Favoris[]
     */
    public function getFavoris()
    {
        return $this->favoris;
    }

    /**
     * Set Bar entity (many to one).
     *
     * @param null|\Biere\BiereBundle\Entity\Bar $bar
     * @return \Biere\BiereBundle\Entity\User
     */
    public function setBar(\Biere\BiereBundle\Entity\Bar $bar = null)
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     * Get Bar entity (many to one).
     *
     * @return null|\Biere\BiereBundle\Entity\Bar
     */
    public function getBar()
    {
        return $this->bar;
    }

    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'         => $this->id,
            'username'   => $this->username,
            'email'      => $this->email,
            'password'   => $this->password,
            'createTime' => $this->createTime ? $this->createTime.'' : $this->createTime,
            'gerant'     => $this->gerant
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }

    /**
     * Add favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     * @return User
     */
    public function addFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * Remove favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     */
    public function removeFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);
    }
}
