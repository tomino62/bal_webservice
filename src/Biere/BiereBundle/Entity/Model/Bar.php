<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Biere\BiereBundle\Entity\Bar
 * @ORM\Table(name="Bar")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\BarRepository")
 */
class Bar
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=45, nullable=false, options={})
     */
    protected $nom;

    /**
     * @var string
     * @ORM\Column(name="image", type="string", length=255, nullable=true, options={})
     */
    protected $image;

    /**
     * @var string
     * @ORM\Column(name="description_fr", type="text", nullable=true, options={})
     */
    protected $descriptionFr;

    /**
     * @var string
     * @ORM\Column(name="description_en", type="text", nullable=true, options={})
     */
    protected $descriptionEn;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true, options={})
     */
    protected $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true, options={})
     */
    protected $longitude;

    /**
     * @var string
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true, options={})
     */
    protected $adresse;

    /**
     * collection of Favoris
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Favoris[]
     * 
     * @ORM\OneToMany(targetEntity="Favoris", mappedBy="bar")
     * @ORM\JoinColumn(name="Bar_id", referencedColumnName="id", nullable=true)
     */
    protected $favoris;

    /**
     * collection of User
     * @var ArrayCollection|\Biere\BiereBundle\Entity\User[]
     * 
     * @ORM\OneToMany(targetEntity="User", mappedBy="bar")
     * @ORM\JoinColumn(name="Bar_id", referencedColumnName="id", nullable=true)
     */
    protected $users;

    /**
     * @var \Biere\BiereBundle\Entity\Biere
	 * @ORM\ManyToMany(targetEntity="Biere", inversedBy="bars", cascade={"persist"})
     * @ORM\JoinColumn(name="Biere_id", referencedColumnName="id", nullable=false)
     */
    protected $bieres;

    /**
     * only construct object
     */
    public function __construct()
    {
        $this->favoris = new ArrayCollection();
        $this->users   = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of nom.
     *
     * @param string $nom
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of latitude.
     *
     * @param string $latitude
     * @return \Biere\BiereBundle\Entity\Bar
     */	
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get the value of latitude.
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }


	    /**
     * Set the value of adresse.
     *
     * @param string $adresse
     * @return \Biere\BiereBundle\Entity\Bar
     */	
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get the value of adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
	
    /**
     * Set the value of longitude.
     *
     * @param string $longitude
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get the value of longitude.
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
	
	
    /**
     * Set the value of image.
     *
     * @param string $image
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get the value of image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of descriptionFr.
     *
     * @param string $descriptionFr
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }

    /**
     * Get the value of descriptionFr.
     *
     * @return string
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * Set the value of descriptionEn.
     *
     * @param string $descriptionEn
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get the value of descriptionEn.
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Add Favoris entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function addFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $favoris->setBar($this);
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * remove Favoris entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function removeFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);

        return $this;
    }

    /**
     * Get Favoris entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Favoris[]
     */
    public function getFavoris()
    {
        return $this->favoris;
    }

    /**
     * Add User entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\User $user
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function addUser(\Biere\BiereBundle\Entity\User $user)
    {
        $user->setBar($this);
        $this->users[] = $user;

        return $this;
    }

    /**
     * remove User entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\User $user
     * @return \Biere\BiereBundle\Entity\Bar
     */
    public function removeUser(\Biere\BiereBundle\Entity\User $user)
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * Get User entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\User[]
     */
    public function getUsers()
    {
        return $this->users;
    }


    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'            => $this->id,
            'nom'           => $this->nom,
            'image'         => $this->image,
            'descriptionFr' => $this->descriptionFr,
            'descriptionEn' => $this->descriptionEn
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }

    /**
     * Add favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     * @return Bar
     */
    public function addFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * Remove favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     */
    public function removeFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);
    }

    /**
     * Add bieres
     *
     * @param \Biere\BiereBundle\Entity\Model\Biere $bieres
     * @return Bar
     */
    public function addBiere(\Biere\BiereBundle\Entity\Model\Biere $bieres)
    {
        $this->bieres[] = $bieres;

        return $this;
    }

    /**
     * Remove bieres
     *
     * @param \Biere\BiereBundle\Entity\Model\Biere $bieres
     */
    public function removeBiere(\Biere\BiereBundle\Entity\Model\Biere $bieres)
    {
        $this->bieres->removeElement($bieres);
    }

    /**
     * Get bieres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBieres()
    {
        return $this->bieres;
    }
}
