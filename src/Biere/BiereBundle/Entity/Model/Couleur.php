<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Biere\BiereBundle\Entity\Couleur
 * @ORM\Table(name="Couleur")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\CouleurRepository")
 */
class Couleur
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="couleur_fr", type="string", length=45, nullable=true, options={})
     */
    protected $couleurFr;

    /**
     * @var string
     * @ORM\Column(name="couleur_en", type="string", length=45, nullable=true, options={})
     */
    protected $couleurEn;

    /**
     * collection of Biere
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Biere[]
     * 
     * @ORM\OneToMany(targetEntity="Biere", mappedBy="couleur")
     * @ORM\JoinColumn(name="Couleur_id", referencedColumnName="id", nullable=false)
     */
    protected $bieres;

    /**
     * only construct object
     */
    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of couleurFr.
     *
     * @param string $couleurFr
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function setCouleurFr($couleurFr)
    {
        $this->couleurFr = $couleurFr;

        return $this;
    }

    /**
     * Get the value of couleurFr.
     *
     * @return string
     */
    public function getCouleurFr()
    {
        return $this->couleurFr;
    }

    /**
     * Set the value of couleurEn.
     *
     * @param string $couleurEn
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function setCouleurEn($couleurEn)
    {
        $this->couleurEn = $couleurEn;

        return $this;
    }

    /**
     * Get the value of couleurEn.
     *
     * @return string
     */
    public function getCouleurEn()
    {
        return $this->couleurEn;
    }

    /**
     * Add Biere entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Biere $biere
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function addBiere(\Biere\BiereBundle\Entity\Biere $biere)
    {
        $biere->setCouleur($this);
        $this->bieres[] = $biere;

        return $this;
    }

    /**
     * remove Biere entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Biere $biere
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function removeBiere(\Biere\BiereBundle\Entity\Biere $biere)
    {
        $this->bieres->removeElement($biere);

        return $this;
    }

    /**
     * Get Biere entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Biere[]
     */
    public function getBieres()
    {
        return $this->bieres;
    }

    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'        => $this->id,
            'couleurFr' => $this->couleurFr,
            'couleurEn' => $this->couleurEn
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }
}
