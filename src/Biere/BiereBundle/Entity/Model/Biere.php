<?php

namespace Biere\BiereBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Biere\BiereBundle\Entity\Biere
 * @ORM\Table(name="Biere")
 * @ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\BiereRepository")
 */
class Biere
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @var nom
     * @ORM\Column(name="nom", type="string", length=255, nullable=false, options={})
     */
    protected $nom;
	
    /**
     * @var float
     * @ORM\Column(name="degre", type="decimal", precision=2, scale=1, nullable=true, options={})
     */
    protected $degre;

    /**
     * @var string
     * @ORM\Column(name="description_fr", type="text", nullable=true, options={})
     */
    protected $descriptionFr;

    /**
     * @var string
     * @ORM\Column(name="description_en", type="text", nullable=true, options={})
     */
    protected $descriptionEn;

    /**
     * @var string
     * @ORM\Column(name="image", type="string", length=255, nullable=true, options={})
     */
    protected $image;

    /**
     * collection of Bar
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Bar[]
     * @ORM\JoinColumn(name="Biere_id", referencedColumnName="id", nullable=false)
     */
    protected $bars;

    /**
     * collection of Favoris
     * @var ArrayCollection|\Biere\BiereBundle\Entity\Favoris[]
     * 
     * @ORM\OneToMany(targetEntity="Favoris", mappedBy="biere")
     * @ORM\JoinColumn(name="Biere_id", referencedColumnName="id", nullable=true)
     */
    protected $favoris;

    /**
     * @var \Biere\BiereBundle\Entity\Couleur
     * @ORM\ManyToOne(targetEntity="Couleur", inversedBy="bieres")
     * @ORM\JoinColumn(name="Couleur_id", referencedColumnName="id", nullable=false)
     */
    protected $couleur;

    /**
     * @var \Biere\BiereBundle\Entity\Marque
     * @ORM\ManyToOne(targetEntity="Marque", inversedBy="bieres")
     * @ORM\JoinColumn(name="Marque_id", referencedColumnName="id", nullable=false)
     */
    protected $marque;

    /**
     * only construct object
     */
    public function __construct()
    {
        $this->bars    = new ArrayCollection();
        $this->favoris = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of degre.
     *
     * @param float $degre
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function setDegre($degre)
    {
        $this->degre = $degre;

        return $this;
    }

    /**
     * Get the value of degre.
     *
     * @return float
     */
    public function getDegre()
    {
        return $this->degre;
    }

    /**
     * Set the value of image.
     *
     * @param string $image
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get the value of image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add Bar entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Bar $bar
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function addBar(\Biere\BiereBundle\Entity\Bar $bar)
    {
        $bar->setBiere($this);
        $this->bars[] = $bar;

        return $this;
    }

    /**
     * remove Bar entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Bar $bar
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function removeBar(\Biere\BiereBundle\Entity\Bar $bar)
    {
        $this->bars->removeElement($bar);

        return $this;
    }

    /**
     * Get Bar entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Bar[]
     */
    public function getBars()
    {
        return $this->bars;
    }

    /**
     * Add Favoris entity to collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function addFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $favoris->setBiere($this);
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * remove Favoris entity from collection (one to many).
     *
     * @param \Biere\BiereBundle\Entity\Favoris $favoris
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function removeFavoris(\Biere\BiereBundle\Entity\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);

        return $this;
    }

    /**
     * Get Favoris entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|\Biere\BiereBundle\Entity\Favoris[]
     */
    public function getFavoris()
    {
        return $this->favoris;
    }

    /**
     * Set Couleur entity (many to one).
     *
     * @param \Biere\BiereBundle\Entity\Couleur $couleur
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function setCouleur(\Biere\BiereBundle\Entity\Couleur $couleur = null)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get Couleur entity (many to one).
     *
     * @return \Biere\BiereBundle\Entity\Couleur
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set Marque entity (many to one).
     *
     * @param \Biere\BiereBundle\Entity\Marque $marque
     * @return \Biere\BiereBundle\Entity\Biere
     */
    public function setMarque(\Biere\BiereBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get Marque entity (many to one).
     *
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * get data as array
     * @return array
     */
    public function toArray()
    {
        return [
            'id'    => $this->id,
            'degre' => $this->degre,
            'image' => $this->image
        ];
    }

    /**
     * to string entity
     * @return string
     * @throws \Symfony\Component\Intl\Exception\MethodNotImplementedException
     */
    public function __toString()
    {
        return (string)$this->nom;
    }

    /**
     * check is new object
     * @return boolean
     */
    public function isNew()
    {
        return !(boolean)$this->id;
    }

    /**
     * Add favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     * @return Biere
     */
    public function addFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris[] = $favoris;

        return $this;
    }

    /**
     * Remove favoris
     *
     * @param \Biere\BiereBundle\Entity\Model\Favoris $favoris
     */
    public function removeFavori(\Biere\BiereBundle\Entity\Model\Favoris $favoris)
    {
        $this->favoris->removeElement($favoris);
    }

    /**
     * Set the value of descriptionFr.
     *
     * @param string $descriptionFr
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }

    /**
     * Get the value of descriptionFr.
     *
     * @return string
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * Set the value of descriptionEn.
     *
     * @param string $descriptionEn
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get the value of descriptionEn.
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set the value of nom.
     *
     * @param string $nom
     * @return \Biere\BiereBundle\Entity\Marque
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
