<?php

namespace Biere\BiereBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BiereRepository extends EntityRepository
{

	public function rechercherBiere($term)
	{	
		$qry = $this	->createQueryBuilder('biere')
							->where('biere.nom LIKE :term')
							->setParameter('term', '%'.$term.'%')
							->OrderBy('	biere.nom')
							
							;
												
		return $qry ->getQuery()->getResult();

	}
	
}