<?php

namespace Biere\BiereBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BarRepository extends EntityRepository
{

	public function rechercherBar($term)
	{	
		$qry = $this	->createQueryBuilder('bar')
							->where('bar.nom LIKE :term')
							->setParameter('term', '%'.$term.'%')
							->OrderBy('	bar.nom')
							
							;
												
		return $qry ->getQuery()->getResult();

	}
}