<?php

namespace Biere\BiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ORM\Table(name="Favoris", indexes={@ORM\Index(name="fk_Favoris_Biere1_idx", columns={"Biere_id"}), ORM\Index(name="fk_Favoris_Bar1_idx", columns={"Bar_id"}), ORM\Index(name="fk_Favoris_User1_idx", columns={"User_id"})})
 * ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\FavorisRepository")
 */
class Favoris extends Model\Favoris
{
}