<?php

namespace Biere\BiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ORM\Table(name="User", indexes={@ORM\Index(name="fk_User_Bar1_idx", columns={"Bar_id"})})
 * ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\UserRepository")
 */
class User extends Model\User
{
}