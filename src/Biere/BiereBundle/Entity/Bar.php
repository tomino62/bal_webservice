<?php

namespace Biere\BiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * ORM\Table(name="Bar", indexes={@ORM\Index(name="fk_Bar_Biere1_idx", columns={"Biere_id"})})
 * ORM\Entity(repositoryClass="CoreBundle\Biere\BiereBundle\Entity\Repository\BarRepository")
 */
class Bar extends Model\Bar
{
}