<?php

namespace Biere\BiereBundle\Form\Model;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BiereType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('degre')
            ->add('descriptionFr')
            ->add('descriptionEn')
            ->add('image')
			->add('couleur', 'entity', array(
				'class' => 'BiereBiereBundle:Model\Couleur',
				'property' => 'couleurFr',
			))
			->add('marque', 'entity', array(
				'class' => 'BiereBiereBundle:Model\Marque',
				'property' => 'nom',
			))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Biere\BiereBundle\Entity\Model\Biere'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'biere_bierebundle_model_biere';
    }
}
