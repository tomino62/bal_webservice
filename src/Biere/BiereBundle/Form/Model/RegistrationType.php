<?php

namespace Biere\BiereBundle\Form\Model;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('gerant')
            ->add('bar')
			;
    }

    public function getName()
    {
        return 'biere_registration';
    }
}
