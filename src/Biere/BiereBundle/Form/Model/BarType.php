<?php

namespace Biere\BiereBundle\Form\Model;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BarType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('image')
            ->add('descriptionFr')
            ->add('descriptionEn')
            ->add('bieres')
			->add('save', 'submit')
			
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Biere\BiereBundle\Entity\Model\Bar'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'biere_bierebundle_model_bar';
    }
}
