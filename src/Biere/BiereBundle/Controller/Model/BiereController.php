<?php

namespace Biere\BiereBundle\Controller\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Biere\BiereBundle\Entity\Model\Biere;
use Biere\BiereBundle\Form\Model\BiereType;

/**
 * Model\Biere controller.
 *
 */
class BiereController extends Controller
{

    public function constuireTableau($bieres)
    {
	
        foreach($bieres as $key => $biere)
        {
                $data[$key] = array(
                                'nom' => "".$biere->getNom()."", 
                                'id' => "".$biere->getId()."",
								'image' => "".$biere->getImage()."",
								'degre' => "".$biere->getDegre()."",
								'descriptionFr' => "".$biere->getDescriptionFr()."",
								'couleur' => "".$biere->getCouleur()->getId()."",
								'marque' => "".$biere->getMarque()->getId()."",																
                );

        }

        if($bieres == null){
                $data = "";
        }
		
		$response = new Response(json_encode($data, JSON_UNESCAPED_SLASHES));
		return $response;
	}

    /**
     * Lists all Model\Biere entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiereBiereBundle:Model\Biere')->findAll();

        return $this->render('BiereBiereBundle:Model/Biere:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Model\Biere entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Biere();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('biere_show', array('id' => $entity->getId())));
        }

        return $this->render('BiereBiereBundle:Model/Biere:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Model\Biere entity.
     *
     * @param Biere $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Biere $entity)
    {
        $form = $this->createForm(new BiereType(), $entity, array(
            'action' => $this->generateUrl('biere_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Model\Biere entity.
     *
     */
    public function newAction()
    {
        $entity = new Biere();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiereBiereBundle:Model/Biere:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Model\Biere entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Biere')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Biere entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Biere:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Model\Biere entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Biere')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Biere entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Biere:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Model\Biere entity.
    *
    * @param Biere $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Biere $entity)
    {
        $form = $this->createForm(new BiereType(), $entity, array(
            'action' => $this->generateUrl('biere_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Model\Biere entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Biere')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Biere entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('biere_edit', array('id' => $id)));
        }

        return $this->render('BiereBiereBundle:Model/Biere:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Model\Biere entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiereBiereBundle:Model\Biere')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Model\Biere entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('biere'));
    }

    /**
     * Creates a form to delete a Model\Biere entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('biere_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function rechercherBiereAction($term)
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere les lettres tapées
       // $term 	= $_GET['term'];

        //	On recupere les bieres dont le nom contient les lettres tapées
        $bieres = $em->getRepository('BiereBiereBundle:Model\Biere')->rechercherBiere($term); 

		$response = $this -> constuireTableau($bieres);
        return $response;
    }
	
	    public function allBiereAction()
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere toutes les bieres
        $bieres = $em->getRepository('BiereBiereBundle:Model\Biere')->findAll();
		
		$response = $this -> constuireTableau($bieres);
		return $response;
    }
	
	public function get15Action()
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere toutes les bieres
        $bieres = $em->getRepository('BiereBiereBundle:Model\Biere')->first15();

		$response = $this -> constuireTableau($bieres);
		return $response;
    }

    public function getAllBieresByCouleurAction($idcouleur)
    {
        $em = $this->getDoctrine()->getManager();

		//	On recupere la couleur voulue avec l'id
		$couleur 	= $em->getRepository('BiereBiereBundle:Model\Couleur')->find($idcouleur); 
		
        //	On recupere les bieres de la couleur voulue
        $bieres 	= $em->getRepository('BiereBiereBundle:Model\Biere')->findByCouleur($couleur); 

		$response = $this -> constuireTableau($bieres);
        return $response;
    }

    public function getAllBieresByMarqueAction($idmarque)
    {
        $em = $this->getDoctrine()->getManager();

		//	On recupere la couleur voulue avec l'id
		$marque 	= $em->getRepository('BiereBiereBundle:Model\Marque')->find($idmarque); 
		
        //	On recupere les bieres de la couleur voulue
        $bieres 	= $em->getRepository('BiereBiereBundle:Model\Biere')->findByMarque($marque); 

		$response = $this -> constuireTableau($bieres);
        return $response;
    }

    public function getAllBieresByBarAction($idbar)
    {
        $em = $this->getDoctrine()->getManager();

		//	On recupere la couleur voulue avec l'id
		$bar 	= $em->getRepository('BiereBiereBundle:Model\Bar')->find($idbar); 
		
		$bieres = $bar->getBieres();
		
		$response = $this -> constuireTableau($bieres);

        return $response;
    }

}
