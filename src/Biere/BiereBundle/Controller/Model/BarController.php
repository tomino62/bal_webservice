<?php

namespace Biere\BiereBundle\Controller\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Biere\BiereBundle\Entity\Model\Bar;
use Biere\BiereBundle\Form\Model\BarType;

/**
 * Model\Bar controller.
 *
 */
class BarController extends Controller
{

    public function constuireTableau($bars)
    {
	
        foreach($bars as $key => $bar)
        {
                $data[$key] = array(
                                'nom' => "".$bar->getNom()."", 
                                'id' => "".$bar->getId()."",
								'adresse' => "".$bar->getAdresse()."",
								'latitude' => "".$bar->getLatitude()."",
								'longitude' => "".$bar->getLongitude()."",
                );

        }

        if($bars == null){
                $data = "";
        }

        $response = new Response(json_encode($data));
        return $response;
	}

    /**
     * Lists all Model\Bar entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiereBiereBundle:Model\Bar')->findAll();

        return $this->render('BiereBiereBundle:Model/Bar:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Model\Bar entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Bar();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bar_show', array('id' => $entity->getId())));
        }

        return $this->render('BiereBiereBundle:Model/Bar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Model\Bar entity.
     *
     * @param Bar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bar $entity)
    {
        $form = $this->createForm(new BarType(), $entity, array(
            'action' => $this->generateUrl('bar_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Model\Bar entity.
     *
     */
    public function newAction()
    {
        $entity = new Bar();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiereBiereBundle:Model/Bar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Model\Bar entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Bar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Bar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

       return $this->render('BiereBiereBundle:Model/Bar:show.html.twig', array(
           'entity'      => $entity,
           'delete_form' => $deleteForm->createView(),
       ));
    }

    /**
     * Displays a form to edit an existing Model\Bar entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Bar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Bar entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Bar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Model\Bar entity.
    *
    * @param Bar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Bar $entity)
    {
        $form = $this->createForm(new BarType(), $entity, array(
            'action' => $this->generateUrl('bar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Model\Bar entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Bar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Bar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('bar_edit', array('id' => $id)));
        }

        return $this->render('BiereBiereBundle:Model/Bar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Model\Bar entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiereBiereBundle:Model\Bar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Model\Bar entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bar'));
    }

    /**
     * Creates a form to delete a Model\Bar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bar_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function rechercherBarAction($term){
	
        $em = $this->getDoctrine()->getManager();

        //	On recupere les bars dont le nom contient les lettres tapées
        $bars = $em->getRepository('BiereBiereBundle:Model\Bar')->rechercherBar($term); 

		$response = $this->constuireTableau($bars);
        return $response;
    }

	
	public function allBarAction(){
	
        $em = $this->getDoctrine()->getManager();

        //	On recupere tous les bars
        $bars = $em->getRepository('BiereBiereBundle:Model\Bar')->findAll();
		
		$response = $this->constuireTableau($bars);
        return $response;
    }

    public function getByBiereNameAction($term){
	
        $em = $this->getDoctrine()->getManager();

        //	On recupere les bars dont le nom contient les lettres tapées
        $bars = $em->getRepository('BiereBiereBundle:Model\Bar')->getByBiereName($term); 

        $response = $this->constuireTableau($bars);
        return $response;
    }

    public function getByBiereIdAction($idbiere){
	
        $em 	= $this->getDoctrine()->getManager();
		
        $bars 	= $em->getRepository('BiereBiereBundle:Model\Bar')->getByBiereId($idbiere);

        $response = $this->constuireTableau($bars);
        return $response;
    }
}
