<?php

namespace Biere\BiereBundle\Controller\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Biere\BiereBundle\Entity\Model\Marque;
use Biere\BiereBundle\Form\Model\MarqueType;

/**
 * Model\Marque controller.
 *
 */
class MarqueController extends Controller
{

    public function constuireTableau($marques)
    {
	
        foreach($marques as $key => $marque)
        {
                $data[$key] = array(
                                'nom' => "".$marque->getNom()."", 
                                'id' => "".$marque->getId()."",															
                );

        }

        if($marques == null){
                $data = "";
        }
		
		$response = new Response(json_encode($data, JSON_UNESCAPED_SLASHES));
		return $response;
	}
	
    /**
     * Lists all Model\Marque entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiereBiereBundle:Model\Marque')->findAll();

        return $this->render('BiereBiereBundle:Model/Marque:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Model\Marque entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Marque();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('marque_show', array('id' => $entity->getId())));
        }

        return $this->render('BiereBiereBundle:Model/Marque:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Model\Marque entity.
     *
     * @param Marque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Marque $entity)
    {
        $form = $this->createForm(new MarqueType(), $entity, array(
            'action' => $this->generateUrl('marque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Model\Marque entity.
     *
     */
    public function newAction()
    {
        $entity = new Marque();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiereBiereBundle:Model/Marque:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Model\Marque entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Marque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Marque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Marque:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Model\Marque entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Marque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Marque entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Marque:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Model\Marque entity.
    *
    * @param Marque $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Marque $entity)
    {
        $form = $this->createForm(new MarqueType(), $entity, array(
            'action' => $this->generateUrl('marque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Model\Marque entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Marque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Marque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('marque_edit', array('id' => $id)));
        }

        return $this->render('BiereBiereBundle:Model/Marque:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Model\Marque entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiereBiereBundle:Model\Marque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Model\Marque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('marque'));
    }

    /**
     * Creates a form to delete a Model\Marque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marque_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function rechercherMarqueAction($term)
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere les marques dont le nom contient les lettres tapées
        $marques = $em->getRepository('BiereBiereBundle:Model\Marque')->rechercherMarque($term); 

		$response = $this -> constuireTableau($marques);
        return $response;
    }
	
	    public function allMarqueAction()
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere toutes les marques
        $marques = $em->getRepository('BiereBiereBundle:Model\Marque')->findAll();
		
		$response = $this -> constuireTableau($marques);
		return $response;
    }
}
