<?php

namespace Biere\BiereBundle\Controller\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Biere\BiereBundle\Entity\Model\Couleur;
use Biere\BiereBundle\Form\Model\CouleurType;

/**
 * Model\Couleur controller.
 *
 */
class CouleurController extends Controller
{

    public function constuireTableau($couleurs)
    {
	
        foreach($couleurs as $key => $couleur)
        {
                $data[$key] = array(
                                'nom' => "".$couleur->getCouleurFr()."", 
                                'id' => "".$couleur->getId()."",															
                );

        }

        if($couleurs == null){
                $data = "";
        }
		
		$response = new Response(json_encode($data, JSON_UNESCAPED_SLASHES));
		return $response;
	}
	
    /**
     * Lists all Model\Couleur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BiereBiereBundle:Model\Couleur')->findAll();

        return $this->render('BiereBiereBundle:Model/Couleur:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Model\Couleur entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Couleur();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('couleur_show', array('id' => $entity->getId())));
        }

        return $this->render('BiereBiereBundle:Model/Couleur:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Model\Couleur entity.
     *
     * @param Couleur $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Couleur $entity)
    {
        $form = $this->createForm(new CouleurType(), $entity, array(
            'action' => $this->generateUrl('couleur_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Model\Couleur entity.
     *
     */
    public function newAction()
    {
        $entity = new Couleur();
        $form   = $this->createCreateForm($entity);

        return $this->render('BiereBiereBundle:Model/Couleur:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Model\Couleur entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Couleur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Couleur entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Couleur:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Model\Couleur entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Couleur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Couleur entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BiereBiereBundle:Model/Couleur:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Model\Couleur entity.
    *
    * @param Couleur $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Couleur $entity)
    {
        $form = $this->createForm(new CouleurType(), $entity, array(
            'action' => $this->generateUrl('couleur_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Model\Couleur entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BiereBiereBundle:Model\Couleur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Model\Couleur entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('couleur_edit', array('id' => $id)));
        }

        return $this->render('BiereBiereBundle:Model/Couleur:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Model\Couleur entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BiereBiereBundle:Model\Couleur')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Model\Couleur entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('couleur'));
    }

    /**
     * Creates a form to delete a Model\Couleur entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('couleur_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function rechercherCouleurAction($term)
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere les couleurs dont le nom contient les lettres tapées
        $couleurs = $em->getRepository('BiereBiereBundle:Model\Couleur')->rechercherCouleur($term); 

		$response = $this -> constuireTableau($couleurs);
        return $response;
    }
	
	    public function allCouleurAction()
    {
        $em = $this->getDoctrine()->getManager();

        //	On recupere toutes les couleurs
        $couleurs = $em->getRepository('BiereBiereBundle:Model\Couleur')->findAll();
		
		$response = $this -> constuireTableau($couleurs);
		return $response;
    }
}
