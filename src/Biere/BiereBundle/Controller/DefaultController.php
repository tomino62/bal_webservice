<?php

namespace Biere\BiereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BiereBiereBundle:Default:index.html.twig', array('name' => $name));
    }
}
